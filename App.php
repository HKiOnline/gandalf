<?php
require_once 'autoloader.php';

$router = new Router();


$router->get("/", function($query) {
	$index = new QuoteController();
	$quote = $index->getQuote();

	$quote_text = $quote["quote"]; // used in index-template
	$quote_author = $quote["author"]; // used in index-template

	header("Content-Type: text/html");
	require 'Views/header-template.php';
	require 'Views/index-template.php';
	require 'Views/footer-template.php';
});

$router->get("/api", function ($query){
	$api = new QuoteController();
	header("Content-Type: application/json");
	echo json_encode($api->getQuote());
});

$router->get("/txt", function ($query){
	$api = new QuoteController();
	$quote = $api->getQuote();

	header("Content-Type: text/plain");
	echo "Gandalf says: \"".$quote["quote"]."\"";
});

$router->fallback(function (){
	header("Content-Type: text/html");
	http_response_code(404);
	echo "<h1>404: You shall not pass!</h1>";
});

?>
