<?php
	spl_autoload_register();

	$basepath = getcwd();

	// Require statements
	require($basepath."/Router.php");
	require($basepath."/Models/QuoteDB.php");
	require($basepath."/Controllers/QuoteController.php");

?>