# What Would Gandalf Say?

Find out with this, absolutely useless web app, what would Gandalf say. The app pulls random quotes from SQLite database and displays them. There is also a JSON API available...for reasons.

Run app with:
		php -S localhost:8090 App.php

After this, head to http://localhost:8090.


## The API

The API is a simple JSON based HTTP API. It is available at http://appurl.tld/api. It responds to HTTP GET requests. As a response it will give a JSON object with the following structure:

		{
			"id":16,
			"quote":"Fly, you fools!",
			"author":"Gandalf the Gray"
		}

The id is a unique identifier for the quote. The quote contains a single quote from Gandalf and finally there is an author-attribute that contains the fictious character who uttered the words. Typically it will be...Gandalf.

## Live demo

There is a live demo for your enjoyment available at [whatwouldgandalfsay.ninja](http://whatwouldgandalfsay.ninja). The demo api is located at [whatwouldgandalfsay.ninja/api](http://whatwouldgandalfsay.ninja/api).

## Supported versions
The app has been seen to work with PHP 5.4, 7.0 and 7.1. Make sure you have the PHP's SQLite-module installed!
