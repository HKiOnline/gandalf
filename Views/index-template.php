<section class='vertical-container'>
	<blockquote id='quote'>
		<h1 id='quote-text'><?php echo $quote_text ?></h1>
		<footer id='quote-author'>— <?php echo  $quote_author ?></footer>
	</blockquote>
</section>