<?php
class QuoteDB extends SQLite3 {
  function __construct($db_path = null){

  	if(is_null($db_path )){
  		$db_path = getcwd()."/Models/quotes.db";
  	}
    $this->open($db_path);
  }

  function getQuote(){
	$result = $this->query("SELECT * FROM Quotes ORDER BY RANDOM() LIMIT 1");
	return $result->fetchArray(SQLITE3_ASSOC);
  }

}

?>
