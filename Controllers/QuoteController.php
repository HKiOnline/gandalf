<?php

class QuoteController{

  protected $quotes_db;
  protected $error;
  
  function __construct(){
        $this->quotes_db = new QuoteDB();
  }

  function getQuote(){



    if($this->quotes_db){
      $quote = $this->quotes_db->getQuote();
      $this->quotes_db->close();
      return $quote;
     
    } else {
      $this->error = $this->quotes_db->lastErrorMsg()."\n";
      return null; 
    }

  }


  function getError(){
    return $this->error;
  }

}

?>
